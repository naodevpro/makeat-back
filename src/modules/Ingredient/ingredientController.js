class IngredientController {
  constructor({
    ingredientService,
    configCloudinary,
    responseHandler,
    ApiError
  }) {
    this.ingredientService = ingredientService;
    this.configCloudinary = configCloudinary;
    this.responseHandler = responseHandler;
    this.ApiError = ApiError;
  }

  getAllIngredients = async (request, response, next) => {
    try {
      let ingredients = await this.ingredientService.getAllIngredients();
      this.responseHandler(
        response,
        201,
        ingredients,
        `Toutes les ingrédients 🥗`
      );
    } catch (err) {
      next(err);
    }
  };

  addIngredient = async (request, response, next) => {
    try {
      if (!request.file) {
        throw new this.ApiError(
          400,
          "Il semble que vous n'ayez pas ajouté d'image à l'ingrédient ❌ "
        );
      }
      const result = await this.configCloudinary.uploader.upload(
        request.file.path
      );
      if (!result) {
        throw new this.ApiError(
          400,
          "Il semble qu'il y ait une erreur lors de l'upload de l'image ❌ "
        );
      }
      let ingredient = await this.ingredientService.addIngredient({
        thumbnail: result.secure_url,
        name: request.body.name
      });
      this.responseHandler(
        response,
        201,
        ingredient,
        `Nouvel ingredient ajoutée ! 🥗`
      );
    } catch (err) {
      next(err);
    }
  };

  getIngredient = async (request, response, next) => {
    try {
      let ingredient = await this.ingredientService.getIngredient(
        request.params.id
      );
      this.responseHandler(response, 201, ingredient);
    } catch (err) {
      next(err);
    }
  };

  deleteIngredient = async (request, response, next) => {
    try {
      let ingredient = await this.ingredientService.deleteIngredient(
        request.params.id
      );
      this.responseHandler(response, 200, ingredient, 'Ingrédient supprimé ✅');
    } catch (err) {
      next(err);
    }
  };
}

export default IngredientController;
