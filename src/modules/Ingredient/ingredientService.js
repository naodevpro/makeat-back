class IngredientService {
  constructor({ ingredientRepository, ApiError }) {
    this.ingredientRepository = ingredientRepository;
    this.apiError = ApiError;
  }

  async getAllIngredients() {
    return await this.ingredientRepository.getIngredients();
  }

  async getIngredient(id) {
    return await this.ingredientRepository.getIngredientById(id);
  }

  async addIngredient(ingredient) {
    return await this.ingredientRepository.createIngredient(ingredient);
  }

  async deleteIngredient(id) {
    return await this.ingredientRepository.deleteIngredientById(id);
  }
}

export default IngredientService;
