class IngredientRepository {
  constructor({ db, ApiError }) {
    this.db = db;
    this.apiError = ApiError;
  }

  async getIngredients() {
    const ingredients = await this.db.Ingredient.findAll();
    if (!ingredients) {
      throw new this.apiError(
        400,
        "Il semble qu'il n'y a aucun ingredients 😖"
      );
    }
    return ingredients;
  }

  async getIngredientById(id) {
    const ingredient = await this.db.Ingredient.findOne({
      where: { id: id }
    });
    if (!ingredient) {
      throw new this.apiError(
        400,
        "Il semble qu'il n'y ai aucun ingredient à cet ID 😖"
      );
    }
    return ingredient;
  }

  async createIngredient(ingredient) {
    const ingredientIsExist = await this.db.Ingredient.findOne({
      where: { name: ingredient.name }
    });
    if (ingredientIsExist) {
      throw new this.apiError(
        400,
        "Il semble qu'il existe deja un ingredient portant le même nom 😖"
      );
    }
    return await this.db.Ingredient.create(ingredient);
  }

  async deleteIngredientById(id) {
    const ingredientIsExist = await this.db.Ingredient.findOne({
      where: { id: id }
    });
    if (!ingredientIsExist) {
      throw new this.apiError(
        400,
        "Il semble que l'ingredient que vous voulez supprimer n'existe pas/plus 😖"
      );
    }
    return await this.db.Ingredient.destroy({ where: { id: id } });
  }
}
export default IngredientRepository;
