'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Ingredient extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.RecipeIngredient, {
        foreignKey: 'ingredientId',
        onDelete: 'CASCADE'
      });
    }
  }
  Ingredient.init(
    {
      thumbnail: DataTypes.STRING,
      name: DataTypes.STRING,
      kcal: DataTypes.INTEGER
    },
    {
      sequelize,
      modelName: 'Ingredient'
    }
  );
  return Ingredient;
};
