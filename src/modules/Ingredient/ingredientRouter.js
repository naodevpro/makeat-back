class IngredientRouter {
  constructor({ router, ingredientController, auth, upload }) {
    this.router = router;
    this.upload = upload;
    this.initializeRoutes({ ingredientController });
    return this.router;
  }
  initializeRoutes({ ingredientController }) {
    this.router
      .route('/ingredients')
      .get(ingredientController.getAllIngredients);
    this.router
      .route('/ingredients/:id')
      .get(ingredientController.getIngredient);
    this.router
      .route('/ingredients')
      .post(
        this.upload.single('thumbnail'),
        ingredientController.addIngredient
      );
    this.router
      .route('/ingredients/:id')
      .delete(ingredientController.deleteIngredient);
  }
}

export default IngredientRouter;
