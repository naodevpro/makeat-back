'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Recipe extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.User, {
        foreignKey: 'userId',
        onDelete: 'CASCADE'
      });
      this.belongsTo(models.Category, {
        foreignKey: 'categoryId',
        onDelete: 'CASCADE'
      });
      this.hasMany(models.RecipeIngredient, {
        foreignKey: 'recipeId',
        onDelete: 'CASCADE'
      });
      this.hasMany(models.Step, {
        foreignKey: 'recipeId',
        onDelete: 'CASCADE'
      });
      this.hasMany(models.Like, {
        foreignKey: 'recipeId',
        onDelete: 'CASCADE'
      });
      this.hasMany(models.Comment, {
        foreignKey: 'recipeId',
        onDelete: 'CASCADE'
      });
      this.hasMany(models.Favorite, {
        foreignKey: 'recipeId',
        onDelete: 'CASCADE'
      });
    }
  }
  Recipe.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      categoryId: DataTypes.INTEGER,
      picture: DataTypes.STRING,
      title: DataTypes.STRING,
      description: DataTypes.STRING,
      time: DataTypes.STRING,
      userId: DataTypes.UUID
    },
    {
      sequelize,
      modelName: 'Recipe'
    }
  );
  return Recipe;
};
