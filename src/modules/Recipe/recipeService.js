class RecipeService {
  constructor({ recipeRepository, ApiError }) {
    this.recipeRepository = recipeRepository;
    this.apiError = ApiError;
  }

  async getAllRecipes() {
    return await this.recipeRepository.getRecipes();
  }

  async getRecipe(id) {
    return await this.recipeRepository.getRecipeById(id);
  }

  async addRecipe(recipe) {
    return await this.recipeRepository.createRecipe(recipe);
  }

  async deleteRecipe(id) {
    return await this.recipeRepository.deleteRecipeById(id);
  }

  async updateRecipe(id, recipe) {
    return await this.recipeRepository.updateRecipeById(id, recipe);
  }
}

export default RecipeService;
