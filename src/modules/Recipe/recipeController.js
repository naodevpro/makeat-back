class RecipeController {
  constructor({ recipeService, configCloudinary, responseHandler, ApiError }) {
    this.recipeService = recipeService;
    this.configCloudinary = configCloudinary;
    this.responseHandler = responseHandler;
    this.ApiError = ApiError;
  }

  getAllRecipes = async (request, response, next) => {
    try {
      let recipes = await this.recipeService.getAllRecipes();
      this.responseHandler(response, 201, recipes, `Toutes les recettes 🥗`);
    } catch (err) {
      next(err);
    }
  };

  addRecipe = async (request, response, next) => {
    try {
      console.log(request.file);
      if (!request.file) {
        throw new this.ApiError(
          400,
          "Il semble que vous n'ayez pas ajouté d'image à la recette ❌ "
        );
      }
      const result = await this.configCloudinary.uploader.upload(
        request.file.path
      );
      if (!result) {
        throw new this.ApiError(
          400,
          "Il semble qu'il y ait une erreur lors de l'upload de l'image ❌ "
        );
      }
      let recipe = await this.recipeService.addRecipe({
        categoryId: request.body.categoryId,
        picture: result.secure_url,
        title: request.body.title,
        description: request.body.description,
        time: request.body.time,
        userId: request.body.userId
      });
      this.responseHandler(
        response,
        201,
        recipe,
        `Nouvelle recette ajoutée ! 🥗`
      );
    } catch (err) {
      next(err);
    }
  };

  getRecipe = async (request, response, next) => {
    try {
      let recipe = await this.recipeService.getRecipe(request.params.id);
      this.responseHandler(response, 201, recipe);
    } catch (err) {
      next(err);
    }
  };

  deleteRecipe = async (request, response, next) => {
    try {
      let recipe = await this.recipeService.deleteRecipe(request.params.id);
      this.responseHandler(response, 200, recipe, 'Recette supprimé ✅');
    } catch (err) {
      next(err);
    }
  };

  updateRecipe = async (request, response, next) => {
    try {
      let recipe = await this.recipeService.updateItem(request.params.id, {
        ...request.body
      });
      this.responseHandler(response, 200, recipe, 'Recette mise à jour ✅');
    } catch (err) {
      next(err);
    }
  };
}

export default RecipeController;
