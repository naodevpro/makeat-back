class RecipeRouter {
  constructor({ router, recipeController, auth, upload }) {
    this.router = router;
    this.upload = upload;
    this.initializeRoutes({ recipeController });
    return this.router;
  }
  initializeRoutes({ recipeController }) {
    this.router.route('/recipes').get(recipeController.getAllRecipes);
    this.router.route('/recipes/:id').get(recipeController.getRecipe);
    this.router
      .route('/recipes')
      .post(this.upload.single('picture'), recipeController.addRecipe);
    this.router.route('/recipes/:id').delete(recipeController.deleteRecipe);
    this.router.route('/recipes/:id').patch(recipeController.updateRecipe);
  }
}

export default RecipeRouter;
