class RecipeRepository {
  constructor({ db, ApiError }) {
    this.db = db;
    this.apiError = ApiError;
  }

  async getRecipes() {
    const recipes = await this.db.Recipe.findAll();
    if (!recipes) {
      throw new this.apiError(400, "Il semble qu'il n'y a aucunes recettes 😖");
    }
    return recipes;
  }

  async getRecipeById(id) {
    console.log('hello');
    console.log(id);
    const recipe = await this.db.Recipe.findOne({
      where: { id: id },
      include: [
        { model: this.db.Step },
        {
          model: this.db.RecipeIngredient
        },
        { model: this.db.Like },
        { model: this.db.Comment },
        { model: this.db.User, attributes: ['id', 'avatar', 'username'] }
      ]
    });
    if (!recipe) {
      throw new this.apiError(
        400,
        "Il semble qu'il n'y ai aucune recette à cet ID 😖"
      );
    }
    return recipe;
  }

  async createRecipe(recipe) {
    const recipeIsExist = await this.db.Recipe.findOne({
      where: { title: recipe.title }
    });
    if (recipeIsExist) {
      throw new this.apiError(
        400,
        "Il semble qu'il existe deja une recette portant le même titre 😖"
      );
    }
    return await this.db.Recipe.create(recipe);
  }

  async deleteRecipeById(id) {
    const recipeIsExist = await this.db.Recipe.findOne({
      where: { id: id }
    });
    if (!recipeIsExist) {
      throw new this.apiError(
        400,
        "Il semble que la recette que vous voulez supprimer n'existe pas/plus 😖"
      );
    }
    return await this.db.Recipe.destroy({ where: { id: id } });
  }

  async updateRecipeById(id, recipe) {
    const recipeIsExist = await this.db.Recipe.findOne({
      where: { id: id }
    });
    if (!recipeIsExist) {
      throw new this.apiError(
        400,
        "Il semble que la recette que vous voulez modifier n'existe pas/plus 😖"
      );
    }
    return await this.db.Recipe.update({ recipe }, { where: { id: id } });
  }
}
export default RecipeRepository;
