'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Like extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.User, {
        foreignKey: 'userId',
        onDelete: 'CASCADE'
      });
      this.belongsTo(models.Recipe, {
        foreignKey: 'recipeId',
        onDelete: 'CASCADE'
      });
    }
  }
  Like.init(
    {
      userId: DataTypes.UUID,
      recipeId: DataTypes.UUID
    },
    {
      sequelize,
      modelName: 'Like'
    }
  );
  return Like;
};
