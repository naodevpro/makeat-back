class LikeRouter {
  constructor({ router, likeController, auth, upload }) {
    this.router = router;
    this.upload = upload;
    this.initializeRoutes({ likeController });
    return this.router;
  }
  initializeRoutes({ likeController }) {
    this.router
      .route('/likes/:recipeId')
      .get(likeController.getAllLikesByRecipe);
    this.router.route('/likes').post(likeController.addLike);
    this.router
      .route('/likes/:recipeId/:userId')
      .delete(likeController.deleteLikeByRecipe);
  }
}

export default LikeRouter;
