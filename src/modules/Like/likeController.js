class LikeController {
  constructor({ likeService, configCloudinary, responseHandler, ApiError }) {
    this.likeService = likeService;
    this.configCloudinary = configCloudinary;
    this.responseHandler = responseHandler;
    this.ApiError = ApiError;
  }

  getAllLikesByRecipe = async (request, response, next) => {
    try {
      let likes = await this.likeService.getAllLikesByRecipe(
        request.params.recipeId
      );
      this.responseHandler(response, 201, likes, `Tous les likes ♥️`);
    } catch (err) {
      next(err);
    }
  };

  addLike = async (request, response, next) => {
    try {
      let like = await this.likeService.addLike({
        ...request.body
      });
      this.responseHandler(response, 201, like, `Nouveau like ajouté ! ❤`);
    } catch (err) {
      next(err);
    }
  };

  deleteLikeByRecipe = async (request, response, next) => {
    try {
      let like = await this.likeService.deleteLikeByRecipe(
        request.params.recipeId,
        request.params.userId
      );
      this.responseHandler(response, 200, like, 'Like supprimé ✅');
    } catch (err) {
      next(err);
    }
  };
}

export default LikeController;
