class LikeService {
  constructor({ likeRepository, ApiError }) {
    this.likeRepository = likeRepository;
    this.apiError = ApiError;
  }

  async getAllLikesByRecipe(recipeId) {
    return await this.likeRepository.getAllLikesByRecipe(recipeId);
  }

  async addLike(like) {
    return await this.likeRepository.createLike(like);
  }

  async deleteLikeByRecipe(recipeId, userId) {
    return await this.likeRepository.deleteLikeByRecipe(recipeId, userId);
  }
}

export default LikeService;
