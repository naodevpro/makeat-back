class LikeRepository {
  constructor({ db, ApiError }) {
    this.db = db;
    this.apiError = ApiError;
  }

  async getAllLikesByRecipe(recipeId) {
    const likes = await this.db.Like.findAll({
      where: { recipeId: recipeId }
    });
    if (!likes) {
      throw new this.apiError(400, "Il semble qu'il n'y a aucuns likes 😖");
    }
    return likes;
  }

  async addLike(like) {
    return await this.db.Like.create(like);
  }

  async deleteLikeByRecipe(recipeId, userId) {
    const likeIsExist = await this.db.Like.findOne({
      where: { recipeId: recipeId, userId: userId }
    });
    if (!likeIsExist) {
      throw new this.apiError(
        400,
        "Il semble que le likea que vous souhaitez supprimer n'existe pas/plus 😖"
      );
    }
    return await this.db.Like.destroy({
      where: { id: likeIsExist.dataValues.id }
    });
  }
}

export default LikeRepository;
