'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.Recipe, {
        foreignKey: 'userId',
        onDelete: 'CASCADE'
      });
      this.hasMany(models.Favorite, {
        foreignKey: 'userId',
        onDelete: 'CASCADE'
      });
      this.hasMany(models.Like, {
        foreignKey: 'userId',
        onDelete: 'CASCADE'
      });
      this.hasMany(models.Comment, {
        foreignKey: 'userId',
        onDelete: 'CASCADE'
      });
      this.hasMany(models.Follow, {
        foreignKey: 'followerId',
        onDelete: 'CASCADE'
      });
      this.hasMany(models.Follow, {
        foreignKey: 'followedId',
        onDelete: 'CASCADE'
      });
    }
  }
  User.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      role: DataTypes.STRING,
      avatar: DataTypes.STRING,
      lastname: DataTypes.STRING,
      firstname: DataTypes.STRING,
      username: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING
    },
    {
      sequelize,
      modelName: 'User'
    }
  );
  return User;
};
