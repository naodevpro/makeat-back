class FavoriteService {
  constructor({ favoriteRepository, ApiError }) {
    this.favoriteRepository = favoriteRepository;
    this.apiError = ApiError;
  }

  async getAllFavoritesByRecipe(recipeId) {
    return await this.favoriteRepository.getAllFavoritesByRecipe(recipeId);
  }

  async getAllFavoritesByUser(userId) {
    return await this.favoriteRepository.getAllFavoritesByUser(userId);
  }

  async addFavorite(favorite) {
    return await this.favoriteRepository.addFavorite(favorite);
  }

  async deleteFavoriteByUser(recipeId, userId) {
    return await this.favoriteRepository.deleteFavoriteByUser(recipeId, userId);
  }
}

export default FavoriteService;
