class FavoriteRepository {
  constructor({ db, ApiError }) {
    this.db = db;
    this.apiError = ApiError;
  }

  async getAllFavoritesByRecipe(recipeId) {
    const favorites = await this.db.Favorite.findAll({
      where: { recipeId: recipeId }
    });
    if (!favorites) {
      throw new this.apiError(400, "Il semble qu'il n'y a aucun favoris 😖");
    }
    return favorites;
  }

  async getAllFavoritesByUser(userId) {
    const favorites = await this.db.Favorite.findAll({
      where: { userId: userId }
    });
    if (!favorites) {
      throw new this.apiError(400, "Il semble qu'il n'y a aucun favoris 😖");
    }
    return favorites;
  }

  async addFavorite(favorite) {
    return await this.db.Favorite.create(favorite);
  }

  async deleteFavoriteByUser(recipeId, userId) {
    const favoriteIsExist = await this.db.Favorite.findOne({
      where: { recipeId: recipeId, userId: userId }
    });
    if (!favoriteIsExist) {
      throw new this.apiError(
        400,
        "Il semble que le favoris que vous souhaitez supprimer n'existe pas/plus 😖"
      );
    }
    return await this.db.Favorite.destroy({
      where: { id: favoriteIsExist.dataValues.id }
    });
  }
}

export default FavoriteRepository;
