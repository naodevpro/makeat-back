class FavoriteRouter {
  constructor({ router, favoriteController, auth, upload }) {
    this.router = router;
    this.upload = upload;
    this.initializeRoutes({ favoriteController });
    return this.router;
  }
  initializeRoutes({ favoriteController }) {
    this.router
      .route('/favorites/:recipeId')
      .get(favoriteController.getAllFavoritesByRecipe);
    this.router
      .route('/favorites/:userId')
      .get(favoriteController.getAllFavoritesByUser);
    this.router.route('/favorites').post(favoriteController.addFavorite);
    this.router
      .route('/favorites/:recipeId/:userId')
      .delete(favoriteController.deleteFavoriteByUser);
  }
}

export default FavoriteRouter;
