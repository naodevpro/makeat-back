'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Favorite extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.User, {
        foreignKey: 'userId',
        onDelete: 'CASCADE'
      });
      this.belongsTo(models.Recipe, {
        foreignKey: 'recipeId',
        onDelete: 'CASCADE'
      });
    }
  }
  Favorite.init(
    {
      userId: DataTypes.UUID,
      recipeId: DataTypes.UUID
    },
    {
      sequelize,
      modelName: 'Favorite'
    }
  );
  return Favorite;
};
