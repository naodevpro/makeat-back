class FavoriteController {
  constructor({
    favoriteService,
    configCloudinary,
    responseHandler,
    ApiError
  }) {
    this.favoriteService = favoriteService;
    this.configCloudinary = configCloudinary;
    this.responseHandler = responseHandler;
    this.ApiError = ApiError;
  }

  getAllFavoritesByRecipe = async (request, response, next) => {
    try {
      let favorites = await this.favoriteService.getAllFavoritesByRecipe(
        request.params.recipeId
      );
      this.responseHandler(
        response,
        201,
        favorites,
        `Tous les favoris de la recette 🏅`
      );
    } catch (err) {
      next(err);
    }
  };

  getAllFavoritesByUser = async (request, response, next) => {
    try {
      let favorites = await this.favoriteService.getAllFavoritesByUser(
        request.params.userId
      );
      this.responseHandler(
        response,
        201,
        favorites,
        `Tous les favoris de l'utilisateur🏅`
      );
    } catch (err) {
      next(err);
    }
  };

  addFavorite = async (request, response, next) => {
    try {
      let favorite = await this.favoriteService.addFavorite({
        ...request.body
      });
      this.responseHandler(
        response,
        201,
        favorite,
        `Nouveau favorite ajouté ! 🏅`
      );
    } catch (err) {
      next(err);
    }
  };

  deleteFavoriteByUser = async (request, response, next) => {
    try {
      let favorite = await this.favoriteService.deleteFavoriteByUser(
        request.params.recipeId,
        request.params.userId
      );
      this.responseHandler(response, 200, favorite, 'Favori supprimé ✅');
    } catch (err) {
      next(err);
    }
  };
}

export default FavoriteController;
