class MakeatRouter {
  constructor({ router }) {
    this.router = router;
    this.initializeRoutes();
    return this.router;
  }

  initializeRoutes() {
    this.router.route('/').get((request, response) => {
      response.json({
        message: "Bienvenue dans l'API de Makeat. 🥗"
      });
    });
  }
}
export default MakeatRouter;
