class CommentService {
  constructor({ commentRepository, ApiError }) {
    this.commentRepository = commentRepository;
    this.apiError = ApiError;
  }

  async getAllCommentsByRecipe(recipeId) {
    return await this.commentRepository.getAllCommentsByRecipe(recipeId);
  }

  async addComment(comment) {
    return await this.commentRepository.addComment(comment);
  }

  async deleteCommentByRecipe(recipeId, userId) {
    return await this.commentRepository.deleteCommentByRecipe(recipeId, userId);
  }
}

export default CommentService;
