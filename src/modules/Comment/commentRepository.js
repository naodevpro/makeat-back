class CommentRepository {
  constructor({ db, ApiError }) {
    this.db = db;
    this.apiError = ApiError;
  }

  async getAllCommentsByRecipe(recipeId) {
    const comments = await this.db.Comment.findAll({
      where: { recipeId: recipeId }
    });
    if (!comments) {
      throw new this.apiError(400, "Il semble qu'il n'y a aucunes comments 😖");
    }
    return comments;
  }

  async addComment(comment) {
    return await this.db.Comment.create(comment);
  }

  async deleteCommentByRecipe(recipeId, userId) {
    const commentIsExist = await this.db.Comment.findOne({
      where: { recipeId: recipeId, userId: userId }
    });
    if (!commentIsExist) {
      throw new this.apiError(
        400,
        "Il semble que le commentaire que vous souhaitez supprimer n'existe pas/plus 😖"
      );
    }
    return await this.db.Comment.destroy({
      where: { id: commentIsExist.dataValues.id }
    });
  }
}

export default CommentRepository;
