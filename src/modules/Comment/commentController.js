class CommentController {
  constructor({ commentService, configCloudinary, responseHandler, ApiError }) {
    this.commentService = commentService;
    this.configCloudinary = configCloudinary;
    this.responseHandler = responseHandler;
    this.ApiError = ApiError;
  }

  getAllCommentsByRecipe = async (request, response, next) => {
    try {
      let comments = await this.commentService.getAllCommentsByRecipe(
        request.params.recipeId
      );
      this.responseHandler(response, 201, comments, `Tous les commentaires 💬`);
    } catch (err) {
      next(err);
    }
  };

  addComment = async (request, response, next) => {
    try {
      let comment = await this.commentService.addComment({
        ...request.body
      });
      this.responseHandler(
        response,
        201,
        comment,
        `Nouveau commentaire ajouté ! 💬`
      );
    } catch (err) {
      next(err);
    }
  };

  deleteCommentByRecipe = async (request, response, next) => {
    try {
      let comment = await this.commentService.deleteCommentByRecipe(
        request.params.recipeId,
        request.params.userId
      );
      this.responseHandler(response, 200, comment, 'Commentaire supprimé ✅');
    } catch (err) {
      next(err);
    }
  };
}

export default CommentController;
