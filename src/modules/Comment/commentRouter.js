class CommentRouter {
  constructor({ router, commentController, auth, upload }) {
    this.router = router;
    this.upload = upload;
    this.initializeRoutes({ commentController });
    return this.router;
  }
  initializeRoutes({ commentController }) {
    this.router
      .route('/comments/:recipeId')
      .get(commentController.getAllCommentsByRecipe);
    this.router.route('/comments').post(commentController.addComment);
    this.router
      .route('/comments/:recipeId/:userId')
      .delete(commentController.deleteCommentByRecipe);
  }
}

export default CommentRouter;
