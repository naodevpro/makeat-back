class RecipeIngredientRouter {
  constructor({ router, recipeIngredientController, auth, upload }) {
    this.router = router;
    this.upload = upload;
    this.initializeRoutes({ recipeIngredientController });
    return this.router;
  }
  initializeRoutes({ recipeIngredientController }) {
    this.router
      .route('/recipeIngredients/:recipeId')
      .get(recipeIngredientController.getAllRecipeIngredientsByRecipe);
    this.router
      .route('/recipeIngredients/:recipeId')
      .post(recipeIngredientController.addRecipeIngredient);
    this.router
      .route('/recipeIngredients/:recipeId')
      .delete(recipeIngredientController.deleteRecipeIngredient);
  }
}

export default RecipeIngredientRouter;
