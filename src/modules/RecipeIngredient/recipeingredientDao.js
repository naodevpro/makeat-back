'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RecipeIngredient extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Recipe, {
        foreignKey: 'recipeId',
        onDelete: 'CASCADE'
      });
      this.belongsTo(models.Ingredient, {
        foreignKey: 'ingredientId',
        onDelete: 'CASCADE'
      });
    }
  }
  RecipeIngredient.init(
    {
      recipeId: DataTypes.UUID,
      ingredientId: DataTypes.INTEGER,
      quantity: DataTypes.INTEGER
    },
    {
      sequelize,
      modelName: 'RecipeIngredient'
    }
  );
  return RecipeIngredient;
};
