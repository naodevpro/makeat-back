class RecipeIngredientService {
  constructor({ recipeIngredientRepository, ApiError }) {
    this.recipeIngredientRepository = recipeIngredientRepository;
    this.apiError = ApiError;
  }

  async getAllRecipeIngredientsByRecipe(recipeId) {
    return await this.recipeIngredientRepository.getAllRecipeIngredientsByRecipe(
      recipeId
    );
  }

  async addRecipeIngredient(recipeId, ingredients) {
    return await this.recipeIngredientRepository.addRecipeIngredient(
      recipeId,
      ingredients
    );
  }

  async deleteRecipeIngredient(recipeId, recipeIngredientId) {
    return await this.recipeIngredientRepository.deleteRecipeIngredient(
      recipeId,
      recipeIngredientId
    );
  }
}

export default RecipeIngredientService;
