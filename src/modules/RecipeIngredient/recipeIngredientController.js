class RecipeController {
  constructor({ recipeIngredientService, responseHandler, ApiError }) {
    this.recipeIngredientService = recipeIngredientService;
    this.responseHandler = responseHandler;
    this.ApiError = ApiError;
  }

  getAllRecipeIngredientsByRecipe = async (request, response, next) => {
    try {
      let recipesIngredients =
        await this.recipeIngredientService.getAllRecipeIngredientsByRecipe(
          request.params.recipeId
        );
      this.responseHandler(
        response,
        201,
        recipesIngredients,
        `Toutes les ingrédients de la recette 🥗`
      );
    } catch (err) {
      next(err);
    }
  };

  addRecipeIngredient = async (request, response, next) => {
    try {
      let recipeIngredient =
        await this.recipeIngredientService.addRecipeIngredient(
          request.params.recipeId,
          request.body.ingredients
        );
      this.responseHandler(
        response,
        201,
        recipeIngredient,
        `Nouvel ingrédient ajouté à la recette ! 🥗`
      );
    } catch (err) {
      next(err);
    }
  };

  deleteRecipeIngredient = async (request, response, next) => {
    try {
      let recipeIngredientDeleted =
        await this.recipeIngredientService.deleteRecipe(
          request.params.recipeId,
          request.body.recipeIngredientId
        );
      this.responseHandler(
        response,
        200,
        recipeIngredientDeleted,
        'Ingrédient de la recette supprimé ✅'
      );
    } catch (err) {
      next(err);
    }
  };
}

export default RecipeController;
