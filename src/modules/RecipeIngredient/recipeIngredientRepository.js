class RecipeIngredientRepository {
  constructor({ db, ApiError }) {
    this.db = db;
    this.apiError = ApiError;
  }

  async getAllRecipeIngredientsByRecipe(recipeId) {
    const recipeIngredients = await this.db.RecipeIngredient.findAll({
      where: { recipeId: recipeId }
    });
    if (!recipeIngredients) {
      throw new this.apiError(
        400,
        "Il semble qu'il n'y a aucun ingrédients pour cette recette 😖"
      );
    }
    return recipeIngredients;
  }

  async addRecipeIngredient(recipeId, ingredients) {
    const recipeIsExist = await this.db.Recipe.findOne({
      where: { id: recipeId }
    });
    if (!recipeIsExist) {
      throw new this.apiError(
        400,
        "Il semble qu'il n'y ai aucune recette créée à cet ID 😖"
      );
    }

    ingredients.map(async (ingredient) => {
      return await this.db.Ingredient.create({
        recipeId: recipeId,
        ingredientId: ingredient.ingredientId,
        quantity: ingredient.quantity
      });
    });
  }

  async deleteRecipeIngredient(recipeId, recipeIngredientId) {
    const recipeIsExist = await this.db.Recipe.findOne({
      where: { id: recipeId }
    });
    if (!recipeIsExist) {
      throw new this.apiError(
        400,
        "Il semble que la recette que vous modifier n'existe pas/plus 😖"
      );
    }
    return await this.db.RecipeIngredient.destroy({
      where: { recipeId: recipeId, id: recipeIngredientId }
    });
  }
}
export default RecipeIngredientRepository;
