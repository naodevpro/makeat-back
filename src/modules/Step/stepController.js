class StepController {
  constructor({ stepService, responseHandler, ApiError }) {
    this.stepService = stepService;
    this.responseHandler = responseHandler;
    this.ApiError = ApiError;
  }

  getAllStepsByRecipe = async (request, response, next) => {
    try {
      let steps = await this.stepService.getAllStepsByRecipe(
        request.params.recipeId
      );
      this.responseHandler(
        response,
        201,
        steps,
        `Toutes les étapes de la recette 🥗`
      );
    } catch (err) {
      next(err);
    }
  };

  addStepByRecipe = async (request, response, next) => {
    try {
      let step = await this.stepService.addStep(request.params.recipeId, {
        ...request.body
      });
      this.responseHandler(
        response,
        201,
        step,
        `Nouvelle étape ajoutée à la recette ! 🥗`
      );
    } catch (err) {
      next(err);
    }
  };

  deleteStep = async (request, response, next) => {
    try {
      let stepDeleted = await this.stepService.deleteStep(
        request.params.recipeId,
        request.body.stepId
      );
      this.responseHandler(response, 200, stepDeleted, 'Étape supprimé ✅');
    } catch (err) {
      next(err);
    }
  };
}

export default StepController;
