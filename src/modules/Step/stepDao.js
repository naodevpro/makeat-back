'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Step extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.Recipe, {
        foreignKey: 'recipeId',
        onDelete: 'CASCADE'
      });
    }
  }
  Step.init(
    {
      number: DataTypes.INTEGER,
      content: DataTypes.STRING,
      recipeId: DataTypes.UUID
    },
    {
      sequelize,
      modelName: 'Step'
    }
  );
  return Step;
};
