class StepService {
  constructor({ stepRepository, ApiError }) {
    this.stepRepository = stepRepository;
    this.apiError = ApiError;
  }

  async getAllStepsByRecipe(recipeId) {
    return await this.stepRepository.getAllStepsByRecipe(recipeId);
  }

  async addStepByRecipe(recipeId, step) {
    return await this.stepRepository.addStepByRecipe(recipeId, step);
  }

  async deleteStep(recipeId, stepId) {
    return await this.stepRepository.deleteStep(recipeId, stepId);
  }
}

export default StepService;
