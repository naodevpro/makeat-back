class StepRepository {
  constructor({ db, ApiError }) {
    this.db = db;
    this.apiError = ApiError;
  }

  async getAllStepsByRecipe(recipeId) {
    const steps = await this.db.Step.findAll({
      where: { recipeId: recipeId }
    });
    if (!steps) {
      throw new this.apiError(
        400,
        "Il semble qu'il n'y a aucun étape dans cette recette 😖"
      );
    }
    return steps;
  }

  async addStepByRecipe(recipeId, step) {
    const stepIsExist = await this.db.Step.findOne({
      where: { content: step.content }
    });
    const recipeIsExist = await this.db.Recipe.findOne({
      where: { id: step.recipeId }
    });
    if (stepIsExist) {
      throw new this.apiError(
        400,
        "Il semble qu'il existe deja une étape similaire 😖"
      );
    }
    return await this.db.Step.create({
      number: step.number,
      content: step.content,
      recipeId: recipeId
    });
  }

  async deleteStep(recipeId, stepId) {
    const stepIsExist = await this.db.Step.findOne({
      where: { id: stepId }
    });
    if (!stepIsExist) {
      throw new this.apiError(
        400,
        "Il semble que l'étape que vous voulez supprimer n'existe pas/plus 😖"
      );
    }
    return await this.db.Step.destroy({
      where: { id: stepId, recipeId: recipeId }
    });
  }
}
export default StepRepository;
