class StepRouter {
  constructor({ router, stepController, auth, upload }) {
    this.router = router;
    this.upload = upload;
    this.initializeRoutes({ stepController });
    return this.router;
  }
  initializeRoutes({ stepController }) {
    this.router
      .route('/steps/:recipeId')
      .get(stepController.getAllStepsByRecipe);
    this.router.route('/steps/:recipeId').post(stepController.addStepByRecipe);
    this.router.route('/steps/:stepId').delete(stepController.deleteStep);
  }
}

export default StepRouter;
