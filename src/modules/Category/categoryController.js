class CategoryController {
  constructor({
    categoryService,
    configCloudinary,
    responseHandler,
    ApiError
  }) {
    this.categoryService = categoryService;
    this.configCloudinary = configCloudinary;
    this.responseHandler = responseHandler;
    this.ApiError = ApiError;
  }

  getAllCategories = async (request, response, next) => {
    try {
      let categories = await this.categoryService.getAllCategories();
      this.responseHandler(
        response,
        201,
        categories,
        `Toutes les catégories 🥗`
      );
    } catch (err) {
      next(err);
    }
  };

  addCategory = async (request, response, next) => {
    try {
      console.log(request.file);
      if (!request.file) {
        throw new this.ApiError(
          400,
          "Il semble que vous n'ayez pas ajouté d'image à la catégorie ❌ "
        );
      }
      const result = await this.configCloudinary.uploader.upload(
        request.file.path
      );
      if (!result) {
        throw new this.ApiError(
          400,
          "Il semble qu'il y ait une erreur lors de l'upload de l'image ❌ "
        );
      }
      let category = await this.categoryService.addCategory({
        thumbnail: result.secure_url,
        name: request.body.name
      });
      this.responseHandler(
        response,
        201,
        category,
        `Nouvelle categorie ajoutée ! 🥗`
      );
    } catch (err) {
      next(err);
    }
  };

  getCategory = async (request, response, next) => {
    try {
      let category = await this.categoryService.getCategory(request.params.id);
      this.responseHandler(response, 201, category);
    } catch (err) {
      next(err);
    }
  };

  deleteCategory = async (request, response, next) => {
    try {
      let category = await this.categoryService.deleteCategory(
        request.params.id
      );
      this.responseHandler(response, 200, category, 'Catégorie supprimé ✅');
    } catch (err) {
      next(err);
    }
  };

  updateCategory = async (request, response, next) => {
    try {
      let category = await this.categoryService.updateItem(request.params.id, {
        ...request.body
      });
      this.responseHandler(response, 200, category, 'Catégorie mise à jour ✅');
    } catch (err) {
      next(err);
    }
  };
}

export default CategoryController;
