class CategoryRepository {
  constructor({ db, ApiError }) {
    this.db = db;
    this.apiError = ApiError;
  }

  async getCategories() {
    const categories = await this.db.Category.findAll();
    if (!categories) {
      throw new this.apiError(
        400,
        "Il semble qu'il n'y a aucunes categories 😖"
      );
    }
    return categories;
  }

  async getCategoryById(id) {
    const category = await this.db.Category.findOne({
      where: { id: id }
    });
    if (!category) {
      throw new this.apiError(
        400,
        "Il semble qu'il n'y ai aucune categorie à cet ID😖"
      );
    }
    return category;
  }

  async createCategory(category) {
    const categoryIsExist = await this.db.Category.findOne({
      where: { name: category.name }
    });
    if (categoryIsExist) {
      throw new this.apiError(
        400,
        "Il semble qu'il existe deja une categorie portant le même titre 😖"
      );
    }
    return await this.db.Category.create(category);
  }

  async deleteCategoryById(id) {
    const categoryIsExist = await this.db.Category.findOne({
      where: { id: id }
    });
    if (!categoryIsExist) {
      throw new this.apiError(
        400,
        "Il semble que la categorie que vous voulez supprimer n'existe pas/plus 😖"
      );
    }
    return await this.db.Category.destroy({ where: { id: id } });
  }

  async updateCategoryById(id, category) {
    const categoryIsExist = await this.db.Category.findOne({
      where: { id: id }
    });
    if (!categoryIsExist) {
      throw new this.apiError(
        400,
        "Il semble que la catégorie que vous voulez modifier n'existe pas/plus 😖"
      );
    }
    return await this.db.Category.update({ category }, { where: { id: id } });
  }
}
export default CategoryRepository;
