class CategoryService {
  constructor({ categoryRepository, ApiError }) {
    this.categoryRepository = categoryRepository;
    this.apiError = ApiError;
  }

  async getAllCategories() {
    return await this.categoryRepository.getCategories();
  }

  async getCategory(id) {
    return await this.categoryRepository.getCategoryById(id);
  }

  async addCategory(category) {
    return await this.categoryRepository.createCategory(category);
  }

  async deleteCategory(id) {
    return await this.categoryRepository.deleteCategoryById(id);
  }

  async updateCategory(id, category) {
    return await this.categoryRepository.updateCategoryById(id, category);
  }
}

export default CategoryService;
