class CategoryRouter {
  constructor({ router, categoryController, auth, upload }) {
    this.router = router;
    this.upload = upload;
    this.initializeRoutes({ categoryController });
    return this.router;
  }
  initializeRoutes({ categoryController }) {
    this.router.route('/categories').get(categoryController.getAllCategories);
    this.router.route('/categories/:id').get(categoryController.getCategory);
    this.router
      .route('/categories')
      .post(this.upload.single('thumbnail'), categoryController.addCategory);
    this.router
      .route('/categories/:id')
      .delete(categoryController.deleteCategory);
    this.router
      .route('/categories/:id')
      .patch(categoryController.updateCategory);
  }
}

export default CategoryRouter;
