class FollowRouter {
  constructor({ router, followController, auth, upload }) {
    this.router = router;
    this.upload = upload;
    this.initializeRoutes({ followController });
    return this.router;
  }
  initializeRoutes({ followController }) {
    this.router
      .route('/follows/:userId')
      .get(followController.getAllFollowersByUser);
    this.router
      .route('/follows/:userId')
      .get(followController.getAllFollowingsByUser);
    this.router
      .route('/follows/:userFollowedId/:userFollowerId')
      .post(followController.followUser);
    this.router
      .route('/follows/:userFollowedId/:userFollowerId')
      .delete(followController.unfollowUser);
  }
}

export default FollowRouter;
