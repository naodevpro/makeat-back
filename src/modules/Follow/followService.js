class FollowService {
  constructor({ followRepository, ApiError }) {
    this.followRepository = followRepository;
    this.apiError = ApiError;
  }

  async getAllFollowersByUser(userId) {
    return await this.followRepository.getAllFollowersByUser(userId);
  }

  async getAllFollowingsByUser(userId) {
    return await this.followRepository.getAllFollowingsByUser(userId);
  }

  async followUser(userFollowedId, userFollowerId) {
    return await this.followRepository.followUser(
      userFollowedId,
      userFollowerId
    );
  }

  async unfollowUser(userFollowedId, userFollowerId) {
    return await this.followRepository.unfollowUser(
      userFollowedId,
      userFollowerId
    );
  }
}

export default FollowService;
