'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Follow extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.User, {
        foreignKey: 'followerId',
        onDelete: 'CASCADE'
      });
      this.belongsTo(models.User, {
        foreignKey: 'followedId',
        onDelete: 'CASCADE'
      });
    }
  }
  Follow.init(
    {
      followerId: DataTypes.UUID,
      followedId: DataTypes.UUID
    },
    {
      sequelize,
      modelName: 'Follow'
    }
  );
  return Follow;
};
