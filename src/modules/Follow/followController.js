class FollowController {
  constructor({ followService, responseHandler, ApiError }) {
    this.followService = followService;
    this.responseHandler = responseHandler;
    this.ApiError = ApiError;
  }

  getAllFollowersByUser = async (request, response, next) => {
    try {
      let follows = await this.followService.getAllFollowersByUser(
        request.params.userId
      );
      this.responseHandler(response, 201, follows, `Tous les followers ♥️`);
    } catch (err) {
      next(err);
    }
  };

  getAllFollowingsByUser = async (request, response, next) => {
    try {
      let follows = await this.followService.getAllFollowingsByUser(
        request.params.userId
      );
      this.responseHandler(response, 201, follows, `Tous les abonnements ♥️`);
    } catch (err) {
      next(err);
    }
  };
  followUser = async (request, response, next) => {
    try {
      let follow = await this.followService.addFollow(
        request.params.userFollowedId,
        request.params.userFollowerId
      );
      this.responseHandler(
        response,
        201,
        follow,
        `Vous venez de suivre un profil ! ❤`
      );
    } catch (err) {
      next(err);
    }
  };

  unfollowUser = async (request, response, next) => {
    try {
      let follow = await this.followService.unfollowUser(
        request.params.userFollowedId,
        request.params.userFollowerId
      );
      this.responseHandler(
        response,
        200,
        follow,
        "Vous venez d'unfollow un profil ✅"
      );
    } catch (err) {
      next(err);
    }
  };
}

export default FollowController;
