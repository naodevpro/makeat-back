class FollowRepository {
  constructor({ db, ApiError }) {
    this.db = db;
    this.apiError = ApiError;
  }

  async getAllFollowersByUser(userId) {
    const followers = await this.db.Follow.findAll({
      where: { followedId: userId }
    });
    if (!followers) {
      throw new this.apiError(
        400,
        "Il semble qu'il n'y ai pas encore d'abonnées 😖"
      );
    }
    return followers;
  }

  async getAllFollowingsByUser(userId) {
    const followings = await this.db.Follow.findAll({
      where: { followerId: userId }
    });
    if (!followings) {
      throw new this.apiError(
        400,
        "Il semble qu'il n'y ai pas encore d'abonnements 😖"
      );
    }
    return followings;
  }

  async followUser(userFollowedId, userFollowerId) {
    const follow = await this.db.Follow.findOne({
      followedId: userFollowedId,
      followerId: userFollowerId
    });
    if (follow) {
      this.unfollowUser(userFollowedId, userFollowerId);
    }
    return await this.db.Follow.create({
      where: { followerId: userFollowerId, followedId: userFollowedId },
      followedId: userFollowedId,
      followerId: userFollowerId
    });
  }

  async unfollowUser(userFollowedId, userFollowerId) {
    const isAlreadyUnfollow = await this.db.Follow.findOne({
      where: { followerId: userFollowerId, followedId: userFollowedId }
    });
    if (!isAlreadyUnfollow) {
      this.followUser(userFollowedId, userFollowerId);
    }
    return await this.db.Follow.destroy({
      where: { followerId: userFollowerId, followedId: userFollowedId }
    });
  }
}
export default FollowRepository;
